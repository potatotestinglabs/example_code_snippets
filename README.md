# Example_Code_Snippets

Here are some example code snippets
Here are a couple of code examples from recent projects:

2. Catcomplete_Quick_Search.php 
is a server-side function that produces a data set for the Search box on my space management web-application
the drop-down list is divided into catagories.

you can visit the demo of this web application at
idb-spaces.com
username: userdemo
password: demopass


The python examples are from a django version of part of the same app.
I wrote these just as code examples to see how suitable django would be for small web apps.

3. models.py
4. views.py
5. forms.py
